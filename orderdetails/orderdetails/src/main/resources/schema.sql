DROP TABLE IF EXISTS tblOrder;
DROP TABLE IF EXISTS tblItem;
DROP TABLE IF EXISTS tblOrderItem;
 
CREATE TABLE tblOrder (
  orderId INT PRIMARY KEY,
  orderDate VARCHAR(250),
  orderStatus VARCHAR(250)
);

CREATE TABLE tblItem (
  itemId INT PRIMARY KEY,
  itemName VARCHAR(250),
  itemUnitPrice INT
);

CREATE TABLE tblOrderItem (
  orderId INT ,
  itemId INT ,
  itemQuantity INT
);


  
